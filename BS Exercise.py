import numpy as np
import scipy.stats as si
import sympy as sy
from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os

#Question 1

def European_call(S0, K, T, r, q, vol):
    S0 # S0 = Stock price
    K   # Exercise prices range
    T # T = Time to expiration
    r # r = risk-free interest rate
    q # q = dividend yield
    vol# vol = volatility

    d1 = (np.log(S0 / K) + (r - q + 0.5 * vol ** 2) * T) / (vol * np.sqrt(T))
    d2 = (np.log(S0 / K) + (r - q - 0.5 * vol ** 2) * T) / (vol * np.sqrt(T))

    call = (S0 * np.exp(-q * T) * si.norm.cdf(d1, 0.0, 1.0) - K * np.exp(-r * T) * si.norm.cdf(d2, 0.0, 1.0))

    return call

print(European_call(100.0,150,1,0.01,0.02,0.2))

#Question 2
from scipy.stats import norm

S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC


np.sqrt(deltat) = T/N #time step
i = 100 #number of simulations
discount_factor = np.exp(-r*T) #discount factor


def gen_paths(S0, r, sigma, T, M, I):
    dt = float(T) / M
    paths = np.zeros((M + 1, I), np.float64)
    paths[0] = S0
    for t in range(1, M + 1):
        rand = np.random.standard_normal(I)
        paths[t] = paths[t - 1] * np.exp((r - 0.5 * sigma ** 2) * dt +
                                         sigma * np.sqrt(dt) * rand)
    return paths





C = np.zeros((i-1,1), dtype=np.float16)
for y in range(0,i-1):
    C[y]=np.maximum(S[y,N-1]-K,0)

CallPayoffAverage = np.average(C)
CallPayoff = discount_factor*CallPayoffAverage
print(CallPayoff)
plt.title('Simulations %d Steps %d Sigma %.2f r %.2f S0 %.2f' % (i, N, sigma, r, S0))
plt.xlabel('Steps')
plt.ylabel('Stock Price')
plt.show()